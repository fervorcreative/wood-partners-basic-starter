<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php if ( get_body_class() ) body_class(); ?>>

    <?php wp_body_open(); ?>

    <?php

      $logo_light = get_field('logo', 'option');
      $logo_dark = get_field('logo_dark', 'option');
      $header_bg_color = get_field('header_bg_color', 'option');
      $show_phone = get_field('show_header_phone_number', 'option');
      $phone = get_field('phone_number', 'option');
      $show_schedule_tour = get_field('show_header_schedule_tour', 'option');
      $schedule_tour = get_field('header_cta_button', 'option');
      $text_us = get_field('text_us_url', 'option');
      $live_chat = get_field('live_chat_url', 'option');
      $instagram_link = get_field('instagram_url', 'option');
      $facebook_link = get_field('facebook_url', 'option');
      $show_apply_now = get_field('show_header_apply_now', 'option');
      $apply_now = get_field('apply_now_link', 'option');
      $show_expanded_nav = get_field('show_menu', 'option');

      $show_cta_headline = get_field('show_cta_headline', 'option');
      $cta_headline = get_field('cta_headline', 'option');
      $show_cta_copy = get_field('show_cta_copy', 'option');
      $cta_copy = get_field('cta_copy', 'option');

      $show_expanded_nav_schedule_tour = get_field('show_expanded_nav_schedule_tour', 'option');
      $expanded_nav_schedule_tour = get_field('expanded_nav_schedule_tour', 'option');
      $show_expanded_nav_apply_now = get_field('show_expanded_nav_apply_now', 'option');
      $expanded_nav_apply_now = get_field('expanded_nav_apply_now', 'option');

      $expanded_nav_show_facebook = get_field('expanded_nav_show_facebook', 'options');
      $expanded_nav_facebook_icon_img = get_field('expanded_nav_facebook_icon_img', 'options');
      $expanded_nav_facebook_url = get_field('expanded_nav_facebook_url', 'options');

      $expanded_nav_show_instagram  = get_field('expanded_nav_show_instagram ', 'options');
      $expanded_nav_instagram_icon_img = get_field('expanded_nav_instagram_icon_img', 'options');
      $expanded_nav_instagram_url = get_field('expanded_nav_instagram_url', 'options');

      $expanded_nav_show_yelp = get_field('expanded_nav_show_yelp', 'options');
      $expanded_nav_yelp_icon_img = get_field('expanded_nav_yelp_icon_img', 'options');
      $expanded_nav_yelp_url = get_field('expanded_nav_yelp_url', 'options');

      $expanded_nav_show_google = get_field('expanded_nav_show_google ', 'options');
      $expanded_nav_google_icon_img = get_field('expanded_nav_google_icon_img', 'options');
      $expanded_nav_google_url = get_field('expanded_nav_google_url', 'options');

      $show_promo_banner = get_field('show_promo_banner', 'options');
      $promo_headline = get_field('main_headline', 'options');
      $decor = get_field('decor', 'options');

      $show_background_image = get_field('show_background_image', 'options');
      $background_image = get_field('background_image', 'options');
    ?>



    <div class="site">

      <a href="#main" class="screen-reader-text">Skip to main content</a>

      <section class="offcanvas">
        <div class="grid-container full">
          <div class="grid-x">


              <!-- Left area -->
              <div class="cell offcanvas-left medium-12 large-4">
                <div class="grid-container grid-x grid-padding-x grid-margin-x">
                  <div class="cell large-12 flex-container flex-dir-column align-center">
                    <div class="offcanvas-access-left">
                      <div class="grid-container">

                        <!-- Styling below just for "Tours Coming Soon!" -->
                        <div class="grid-x" style="align-items: center;" >
                          <div class="cell small-12 medium-12 large-12 flex-container align-center">

                            <div class="copy">

                              <!-- <?php if ($show_cta_headline ): ?>
                                <h3><?= $cta_headline ?></h3>
                              <?php endif; ?>
                              <?php if ($show_cta_copy ): ?>
                                <?= $cta_copy ?>
                              <?php endif; ?>                               -->

                              <h3>Tours Coming Soon!</h3>

                              <!-- <?php if ($show_expanded_nav_schedule_tour ): ?>
                                <p><a class="button schedule-tour" href="/contact-us/"><?= $expanded_nav_schedule_tour['title']; ?></a></p>
                              <?php endif; ?>
                              <?php if ($show_expanded_nav_apply_now): ?>
                                <p><a class="button apply-now" href="<?= $apply_now['url']; ?>"><?= $expanded_nav_apply_now['title']; ?></a></p>
                              <?php endif; ?> -->
                            </div> <!-- .copy -->

                            <div class="find-us">
                              <div class="decor1"></div>
                              <div class="decor2"></div>
                            </div> <!-- .find-us -->

                          </div> <!-- .cell -->
                        </div> <!-- .grid-x -->
                      </div> <!-- .grid-container -->
                    </div> <!-- .offcanvas-access-left -->
                  </div> <!-- .cell -->
                </div> <!-- .grid-x -->
              </div> <!-- .left-area -->

              <!-- Right Area -->
              <div class="cell offcanvas-right medium-12 large-8" >
                <div class="grid-container">
                  <div class="grid-x grid-padding-x grid-margin-x">
                    <div class="cell small-12 medium-12 large-12 flex-container flex-dir-column align-center">
                      <div class="offcanvas-access-right">
                        <div class="grid-container">
                          <div class="grid-x align-center ">
                            <div class="cell small-12 medium-12 large-5 flex-container align-center flex-dir-column mobile-links-top">

                              <?php
                                $menu_args = array(
                                  'theme_location'  => 'menu-header',
                                  'container'       => 'nav',
                                  'container_class' => 'flex-container flex-dir-column align-center',
                                  'menu_class'      => 'menu',
                                  'echo'            => true,
                                  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                  'depth'           => 0,
                                 );

                                 wp_nav_menu($menu_args); ?>

                            </div> <!-- .cell -->

                            <div class="cell small-12 medium-12 large-5 flex-container align-center flex-dir-column mobile-links-bottom align-middle">

                             <?php
                                $menu_args = array(
                                  'theme_location'  => 'menu-header-secondary',
                                  'container'       => 'div',
                                  'menu_class'      => 'links',
                                  'echo'            => true,
                                  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                  'depth'           => 0,
                                 );

                                 wp_nav_menu($menu_args); ?>

                              <div class="review-links">

                                <!-- <div>
                                  <?php if ($expanded_nav_show_facebook): ?>
                                    <a href="<?= $expanded_nav_facebook_url; ?>"><img src="<?= $expanded_nav_facebook_icon_img['url']  ?>"/></a>
                                  <?php endif; ?>
                                  <?php if ($expanded_nav_show_instagram): ?>
                                    <a href="<?= $expanded_nav_instagram_url; ?>"><img src="<?= $expanded_nav_instagram_icon_img['url']  ?>"/></a>
                                  <?php endif; ?>
                                </div>
                                <div>
                                  <?php if ($expanded_nav_show_yelp): ?>
                                    <a href="<?= $expanded_nav_yelp_url; ?>"><img src="<?= $expanded_nav_yelp_icon_img['url']  ?>"/></a>
                                  <?php endif; ?>
                                  <?php if ($expanded_nav_show_google): ?>
                                    <a href="<?= $expanded_nav_google_url; ?>"><img src="<?= $expanded_nav_google_icon_img['url']  ?>"/></a>
                                  <?php endif; ?>
                                </div> -->

                              </div>
                            </div> <!-- .grid-x -->
                          </div> <!-- .grid-container -->
                        </div> <!-- .offcanvas-access -->
                      </div> <!-- .cell -->
                    </div> <!-- .cell -->
                  </div> <!-- .grid-x -->
                </div> <!-- .grid-container -->
              </div> <!-- .cell -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->
      </section>

      <?php if ($show_promo_banner): ?>
          <div class="promo-bar">
            <div class="promo">
              <h6><?php echo $promo_headline; ?></h6>
            </div>
          </div>
      <?php endif; ?>

      <style>
        .header { background-color: <?= $header_bg_color; ?>; }
      </style>

      <header class="header" role="banner">
        <?php if (!empty($decor)): ?>
          <img class="decor" src="<?= $decor['url']; ?>" alt="<?= $decor['alt']; ?>" />
        <?php endif; ?>
        <div class="grid-container fluid">
          <div class="grid-x grid-padding-x align-justify align-middle">


            <div class="cell small-6 medium-4 large-4 flex-container">
              <a class="header__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img class="logo-light" src="<?= $logo_light['url']; ?>" alt="<?= $logo_light['alt']; ?>" />
              </a>
            </div>

            <div class="cell small-6 medium-8 large-7 flex-container align-middle align-right header-right">
              <!-- <?php
                $menu_args = array(
                  'theme_location'  => 'menu-header-secondary',
                  'container'       => 'nav',
                  'menu_class'      => 'links',
                  'echo'            => true,
                  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                  'depth'           => 1,
                  );

                  wp_nav_menu($menu_args);
              ?> -->

                <?php if ($show_phone): ?>
                  <a class="header-links header-link-padding" href="<?= $phone['url'] ?>"><?= $phone['title'] ?></a></a>
                <?php endif; ?>

                <?php if ($show_schedule_tour): ?>
                  <a class="header-links" href="<?= $schedule_tour['url'] ?>"><button class="button">SCHEDULE A TOUR</button></a>
                <?php endif; ?>

                <?php if ($show_apply_now): ?>
                  <a class="header-links header-link-padding" href="<?= $apply_now['url'] ?>"><button class="button">APPLY NOW</button></a>
                <?php endif; ?>


                <?php if ($show_expanded_nav): ?>

                <div class="align-middle header-menu">
                  <a class="nav-link js-nav-toggle" href="javascript:void(0)" role="button" aria-pressed="false">
                    <div class="nav-link__outer">
                      <div class="nav-link__icon"></div>
                    </div>
                  </a>
                </div>

                <?php endif; ?>

            </div>

          </div>
        </div>
      </header>
