<?php
/**
 * Page template file
 */

get_header(); ?>

<main id="main" role="main">

  <?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'partials/content', 'modules' ); ?>
    <?php endwhile; ?>
  <?php else : ?>
    <?php get_template_part( 'partials/content', 'none' ); ?>
  <?php endif; ?>
</main>

<?php get_footer(); ?>
