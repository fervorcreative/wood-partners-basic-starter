/* global AOS, AppData, google, gsap, mapStyles, mixitup */

// Set up App object and jQuery
var App = App || {},
  $ = $ || jQuery;

App.fadeAnimations = function() {
  AOS.init({
    disable: 'mobile',
    easing: 'ease-in-sine',
    duration: 800,
    //once: true,
  });
}

App.navLink = function() {
  $('.js-nav-toggle').on( 'click', function() {
    // Toggle aria attribute
    $(this).attr('aria-pressed', $(this).attr('aria-pressed') === 'false' ? 'true' : 'false')

    // Show/hide nav overlay
    if ($('html').hasClass('is-open')) {
      $('.nav--header').fadeToggle(200);
      $('html').toggleClass('is-open').css('overflow','scroll');
      $('.header__logo .logo-dark').removeClass('dark-logo-active');
      $('.header .decor').removeClass('hidden');
      $('.header').css('z-index','3');
      $('.header-links').removeClass('do-not-display');
    } else {
      $('.nav--header').fadeToggle(250);
      window.setTimeout(function() {
        $('html').toggleClass('is-open').css('overflow','hidden');
      }, 50);
      $('.header__logo .logo-dark').addClass('dark-logo-active');
      $('.header .decor').addClass('hidden');
      $('.header').css('z-index','5');
      $('.header-links').addClass('do-not-display');
    }
  });
};


App.floorPlans = function() {

  window.mixitup('.floor-plan-units', {
    animation: {
      effects: 'fade translateZ(-100px)'
    },
    load: {
      filter : 'all',
      sort: 'order:asc sqft:asc',
    }
  });

  $('.section--floor-plans .mfp-image').magnificPopup({
    type: 'image',
    mainClass: 'mfp-fade',
    gallery: {
      enabled: false,
      navigateByImgClick: true,
      preload: [0,1] // Will preload 0 - before current, and 1 after the current image
    }
  });

  var defaultText = 'View Floor Plan';
  $('a[data="view-plan"]').on('click', function (e) {
    e.preventDefault();
    $('html, body').stop();
    var target = $(this).attr('href');

    $(target).toggleClass('expand');
    $(this).toggleClass('is-active');

    if ($(this).hasClass('is-active')) {
      $(this).text('Hide Floor Plan');
    } else {
      $(this).text(defaultText);
    }

  });

}


App.gallery = function () {
  var section = $('.gallery'),
    slider = section.prev().find('.slider');

  if (!section.length) {
    return;
  }

  var items = $('.gallery-grid .cell'),
    filters = section.find('.filters a');

  filters.on('click', function () {
    var filter = $(this),
      query = filter.data('filter');

    if (filter.hasClass('active')) {
      return;
    }

    filters.removeClass('active');
    filter.addClass('active');

    var tl = gsap.timeline(),
      selected =
        query === '*' ? items : items.filter("[data-filter*='" + query + "']");

    tl.to(items, {
      y: -10,
      opacity: 0,
      duration: 0.25,
      ease: 'expo.out',
    });

    tl.set(selected, {
      y: 10,
      opacity: 0,
      display: 'flex',
    });

    if (items.not(selected).length) {
      tl.set(items.not(selected), {
        display: 'none',
      });
    }

    tl.add(AOS.refresh);

    tl.to(selected, {
      y: 0,
      opacity: 1,
      duration: 0.5,
      ease: 'expo.out',
      stagger: 0.025,
    });

    var currentSlide = section.prev().find('.current-slide'),
      totalSlides = section.prev().find('.total-slides'),
      slides = slider.children().filter('.slide').length
        ? slider.children().filter('.slide')
        : slider.find('.slide'),
      previousSlides = slider.find('.active');

    // Re-initialize the slider with only the active slides
    slider.flickity('destroy');
    slides.hide();
    previousSlides.remove();

    var selectedSlides =
      query === '*' ? slides : slides.filter("[data-filter*='" + query + "']");
    selectedSlides.clone().addClass('active').show().appendTo(slider);

    slider.flickity({
      cellSelector: '.active',
      cellAlign: 'center',
      imagesLoaded: true,
      pageDots: false,
      prevNextButtons: false,
      wrapAround: true,
      adaptiveHeight: true,
    });

    // Update the pagination labels
    currentSlide.text(1);
    totalSlides.text(selectedSlides.length);
  });

  items.on('click', function () {
    var index = items.filter(':visible').index(this);

    slider.flickity('select', index);

    gsap.to(window, {
      duration: 1,
      ease: 'expo.out',
      scrollTo: {
        y: slider,
        offsetY: 240,
        autoKill: true,
      },
    });
  });

};

App.galleryCarousel = function () {
  var sections = $('.gallery-slider');

  sections.each(function () {
    var section = $(this),
      slider = section.find('.slider'),
      controls = section.find('.slider-controls'),
      previous = controls.find('.previous'),
      next = controls.find('.next'),
      pagination = section.find('.slider-pagination'),
      currentSlide = pagination.find('.current-slide');

    if (slider.children().length < 2) {
      controls.remove();
      pagination.remove();
      return;
    }

    slider.flickity({
      cellAlign: 'center',
      imagesLoaded: true,
      pageDots: false,
      prevNextButtons: false,
      wrapAround: true,
      adaptiveHeight: true,
    });

    slider.imagesLoaded(function () {
      slider.addClass('loaded');
      AOS.refresh();
    });

    slider.on('change.flickity', function (event, index) {
      var tl = gsap.timeline();

      tl.to(currentSlide, {
        opacity: 0,
        y: -20,
        duration: 0.25,
        ease: 'expo.in',
      });

      tl.add(function () {
        currentSlide.text(index + 1);
      });

      tl.set(currentSlide, {
        opacity: 0,
        y: 20,
      });

      tl.to(currentSlide, {
        opacity: 1,
        y: 0,
        duration: 1,
        ease: 'expo.out',
      });
    });

    previous.on('click', function () {
      slider.flickity('previous');
    });

    next.on('click', function () {
      slider.flickity('next');
    });
  });
};


App.homeSlider = function() {
  $('.alta-slider').slick({
    arrows: true,
    dots: false,
    infinite: true,
    //centerMode: true,
    slidesToShow: 3,
    prevArrow: '<div class="slick-prev"><img src="/wp-content/themes/fervor-altafh-full/assets/images/arrow-prev.svg" alt="Alta Federal Hill" /></div>',
    nextArrow: '<div class="slick-next"><img src="/wp-content/themes/fervor-altafh-full/assets/images/arrow-next.svg" alt="Alta Federal Hill" /></div>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
}


App.simpleMap = function() {
  // Google Map
	var lat = AppData.lat;
	var lng = AppData.lng;
	var map_zoom = 15;

	var mapOptions = {
		zoom: map_zoom,
		scrollwheel: false,
		saturation: -100,
		center: new google.maps.LatLng(lat, lng),
		disableDefaultUI: true,
		styles: [
			{
				featureType: 'water',
				elementType: 'geometry',
				stylers: [
					{
						color: '#e9e9e9',
					},
					{
						lightness: 17,
					},
				],
			},
			{
				featureType: 'landscape',
				elementType: 'geometry',
				stylers: [
					{
						color: '#f5f5f5',
					},
					{
						lightness: 20,
					},
				],
			},
			{
				featureType: 'road.highway',
				elementType: 'geometry.fill',
				stylers: [
					{
						color: '#ffffff',
					},
					{
						lightness: 17,
					},
				],
			},
			{
				featureType: 'road.highway',
				elementType: 'geometry.stroke',
				stylers: [
					{
						color: '#ffffff',
					},
					{
						lightness: 29,
					},
					{
						weight: 0.2,
					},
				],
			},
			{
				featureType: 'road.arterial',
				elementType: 'geometry',
				stylers: [
					{
						color: '#ffffff',
					},
					{
						lightness: 18,
					},
				],
			},
			{
				featureType: 'road.local',
				elementType: 'geometry',
				stylers: [
					{
						color: '#ffffff',
					},
					{
						lightness: 16,
					},
				],
			},
			{
				featureType: 'poi',
				elementType: 'geometry',
				stylers: [
					{
						color: '#f5f5f5',
					},
					{
						lightness: 21,
					},
				],
			},
			{
				featureType: 'poi.park',
				elementType: 'geometry',
				stylers: [
					{
						color: '#dedede',
					},
					{
						lightness: 21,
					},
				],
			},
			{
				elementType: 'labels.text.stroke',
				stylers: [
					{
						visibility: 'on',
					},
					{
						color: '#ffffff',
					},
					{
						lightness: 16,
					},
				],
			},
			{
				elementType: 'labels.text.fill',
				stylers: [
					{
						saturation: 36,
					},
					{
						color: '#333333',
					},
					{
						lightness: 40,
					},
				],
			},
			{
				elementType: 'labels.icon',
				stylers: [
					{
						visibility: 'off',
					},
				],
			},
			{
				featureType: 'transit',
				elementType: 'geometry',
				stylers: [
					{
						color: '#f2f2f2',
					},
					{
						lightness: 19,
					},
				],
			},
			{
				featureType: 'administrative',
				elementType: 'geometry.fill',
				stylers: [
					{
						color: '#fefefe',
					},
					{
						lightness: 20,
					},
				],
			},
			{
				featureType: 'administrative',
				elementType: 'geometry.stroke',
				stylers: [
					{
						color: '#fefefe',
					},
					{
						lightness: 17,
					},
					{
						weight: 1.2,
					},
				],
			},
		],
	};

	if (jQuery('body').hasClass('contact-us')) {
        var map = new google.maps.Map(
			document.getElementById('map'),
			mapOptions
		);
	}

	var image = {
		url: AppData.template_dir + '/assets/images/map-markers/marker-home.svg',
		//scaledSize: new google.maps.Size(59, 59),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(29.5,29.5)
	};

	var myLatLng = new google.maps.LatLng(lat, lng);

	if (jQuery('body').hasClass('contact-us')) {
		var stetsonMarker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			icon: image,
			url: AppData.map_link,
		});
	}

	if (jQuery('body').hasClass('contact-us')) {
		google.maps.event.addListener(stetsonMarker, 'click', function () {
			window.open(this.url,'_blank');
		});
	}
}

// Google Map Interactive
App.neighborhoodMap = function() {
  var infowindow = new google.maps.InfoWindow({
  content: '',
  });

  function render_map(container) {
  var mapNode = container.find('.map')[0];
  var filters = container.find('.filters button');
  var mobile_filters = container.find('.map-dropdown-menu a');
  var markers = container.find('ul.locations li');
  var args = {
      zoom: 15,
      center: new google.maps.LatLng(0,0),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: mapStyles,
  };

  var map = new google.maps.Map(mapNode, args);

  map.markers = [];

  markers.each(function () {
      add_marker($(this), map, container);
  });

  add_marker_logo(map);

  filters.on('click', function () {
      var filter = $(this).data('filter');
      var bounds = new google.maps.LatLngBounds();

      filters.removeClass('active filter-all');

      if (filter==='*') {
        $(this).addClass('active filter-all');
      } else {
        $(this).addClass('active');
      }

      $.each(map.markers, function (i, marker) {
      if (filter === marker.category || filter === '*') {
          marker.setVisible(true);
          bounds.extend({
          lat: marker.position.lat(),
          lng: marker.position.lng(),
          });
      } else {
          marker.setVisible(false);
      }
      });

      map.fitBounds(bounds);
      infowindow.close();
  });

  // mobile filters
  mobile_filters.on('click', function () {
    var filter = $(this).data('filter');
    var bounds = new google.maps.LatLngBounds();

    filters.removeClass('active filter-all');

    if (filter==='*') {
      $(this).addClass('active filter-all');
    } else {
      $(this).addClass('active');
    }

    $.each(map.markers, function (i, marker) {
    if (filter === marker.category || filter === '*') {
        marker.setVisible(true);
        bounds.extend({
        lat: marker.position.lat(),
        lng: marker.position.lng(),
        });
    } else {
        marker.setVisible(false);
    }
    });

    map.fitBounds(bounds);
    infowindow.close();
});

  center_map(map);

  return map;
  }

  function add_marker($marker, map) {
  var latLng = new google.maps.LatLng(
      $marker.attr('data-lat'),
      $marker.attr('data-lng')
  );

  var icon = {
      url: $marker.attr('data-marker'),
      scaledSize: new google.maps.Size(30, 40),
  };

  var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      icon: icon,
      zIndex: 1,
  });

  var content =
      '<strong>' +
      $marker.data('name') +
      '</strong><br /><span>' +
      $marker.data('address') +
      '</span>';

  marker.category = $marker.data('category');

  map.markers.push(marker);

  $marker.on('click', function () {
      marker.setVisible(true);
      infowindow.setContent(content);
      infowindow.open(map, marker);
      map.panTo(latLng);

      gsap.to(window, {
        duration: 1,
        ease: 'expo.out',
        scrollTo: {
          y: '.map',
          offsetY: 40,
        },
      });
  });

  google.maps.event.addListener(marker, 'click', function () {
      infowindow.setContent(content);
      infowindow.open(map, marker);
      map.panTo(latLng);
  });

  google.maps.event.addListener(map, 'click', function () {
      if (infowindow) {
      infowindow.close();
      }
  });
  }

  function add_marker_logo(map) {
  var icon = {
      url: AppData.template_dir + '/assets/images/map-markers/marker-home.svg',
      anchor: new google.maps.Point(24, 24),
      scaledSize: new google.maps.Size(48, 48)
  };

  var markerLogo = new google.maps.Marker({
      position: new google.maps.LatLng(
      AppData.lat,
      AppData.lng
      ),
      map: map,
      icon: icon,
      zIndex: 0,
  });

  google.maps.event.addListener(markerLogo, 'click', function () {
      window.open(AppData.map_link, '_blank');
  });
  }

  function center_map(map) {
  var bounds = new google.maps.LatLngBounds();

  // Only set the initial bounds to include Restaurants, which are more centrally located
  $.each(map.markers, function (i, marker) {
      // if ("Schools" === marker.category) {
      //   bounds.extend({
      //       lat: marker.position.lat(),
      //       lng: marker.position.lng(),
      //   });
      // }
  });

  // map.setCenter(bounds.getCenter());
  // map.fitBounds(bounds);
  // // Remove one zoom level to ensure no marker is on the edge.


  //map.fitBounds(bounds);
  map.setCenter(new google.maps.LatLng(30.5814602, -97.8508801));
  //map.setCenter(bounds.getCenter());
  map.setZoom(13);

  }

  $('.neighborhood-map').each(function () {
    render_map($(this));
  });
}

App.smoothScroll = function () {
  $('a[href^=\\#]:not([href=\\#])').on('click', function (event) {
    var target = $.attr(this, 'href');
    var targetPosition = $(target).offset().top;
    var currentPosition = $('.site').offset().top;

    $('html, body')
      .stop()
      .animate(
        {
          scrollTop: targetPosition - currentPosition - 300,
        },
        400
      );

    event.preventDefault();
  });
};

App.faq = function(){
  var mixer = mixitup('.faq-list', {
    animation: {
      effects: 'fade translateZ(-100px)'
    },
    load: {
      filter : '*'
    }
  });
};

// Instantiate functions when document is ready
$(document).ready(function() {
  App.navLink();
  App.fadeAnimations();
  App.homeSlider();
  App.smoothScroll();
});

// Instantiate functions when document has loaded most assets such as images
$(window).on('load', function() {
  if ($('body').hasClass('gallery')){
    App.gallery();
    App.galleryCarousel();
  }
  if ($('body').hasClass('floor-plans')){
    App.floorPlans();
  }
  if ($('body').hasClass('faq')){
    App.faq();
  }
  if ($('body').hasClass('neighborhood')){
    App.neighborhoodMap ();
  }
  if ($('body').hasClass('contact-us')){
    App.simpleMap();
  }
});
