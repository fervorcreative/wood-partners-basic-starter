<?php
/**
 * Template part for displaying a message that posts cannot be found.
 */
?>

<section class="section no-results not-found">
	<div class="grid-container">
		<header class="grid-x">
			<div class="cell">
				<h1><?php esc_html_e( 'Nothing Found' ); ?></h1>
			</div>
		</header>

		<div class="grid-x">
			<div class="cell">
				<?php if ( is_search() ) : ?>

					<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.' ); ?></p>

					<?php get_search_form(); ?>

				<?php else : ?>

					<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.' ); ?></p>

					<?php get_search_form(); ?>

				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
