<?php

if (have_rows('content_modules', $id)) {
  while (have_rows('content_modules', $id)) {
    the_row();
    get_template_part('modules/' . get_row_layout());
  }
}

?>
