<?php

/**
 * Created by Estado Digital.
 * RentCafe API
 */

function getRentCafe() {

	$requestType = 'floorplan';
	$apiToken = 'e9137e7e-f4b9-49f0-981e-4923601794c2';
    $propertyCode = get_field('property_id', 'option');
    $apiURL = 'https://api.rentcafe.com/rentcafeapi.aspx?requestType='.$requestType.'&apiToken='.$apiToken.'&propertyCode='.$propertyCode;

	$token          = $apiToken;
	$property_code  = $propertyCode;

	$FLOORPLANS   = get_data('floorplan', $token, $property_code);
	$UNITS 			  = get_data('apartmentavailability', $token, $property_code);

	$formatted_data = formatData($FLOORPLANS, $UNITS);

    // print_r($formatted_data);

	return $formatted_data;
}

function get_data( $service, $token, $property_code ) {

	$arrContextOptions=array(
	        "ssl"=>array(
	        "verify_peer"=>false,
	        "verify_peer_name"=>false,
	    ),
	);
	$data = file_get_contents( 'https://domain.securecafe.com/rentcafeapi.aspx?requestType=' . $service . '&apiToken=' . $token . '&propertycode=' . $property_code, false, stream_context_create($arrContextOptions));

	//var_dump(json_decode($data));

	return json_decode( $data );
}

function formatData($FLOORPLANS, $UNITS) {

	$data = (object)[];

	foreach ($FLOORPLANS as $f => $floorplan) {
		if(isset($floorplan->Error)) break;
		$Units = [];
		foreach ($UNITS as $u => $unit) {
			if(isset($unit->Error)) break;
			if($unit->FloorplanId == $floorplan->FloorplanId) {
				array_push($Units, $unit);
			}
		}

		$Units = (object)$Units;

		

		$floorplan->Units = (object)[];
		$floorplan->Units = $Units;

		if(count(array($Units))) {
      if($floorplan->MinimumRent > 0) {
        $FloorplanName = getFloorPlanName($floorplan->Beds);
        $floorCat = $FloorplanName;
        if (!isset($data->$floorCat)) {
            $data->$floorCat = (object)[];
            $data->$floorCat->Name = $FloorplanName;
            $data->$floorCat->Slug = getFloorPlanSlug($FloorplanName);
            $data->$floorCat->Short = getFloorPlanShort($FloorplanName);
            $data->$floorCat->Order = getFloorPlanOrder($FloorplanName);
            $data->$floorCat->floorplans = [];
        }
        array_push($data->$floorCat->floorplans, $floorplan);
      }
		}
	}

	$data = (array)$data;

	usort($data, "cmp");

	$data = (object)$data;

	return $data;
}

function getPrevNext($current, $DATA) {
	foreach($DATA as $data) {
		$total = count($data->floorplans);
		foreach($data->floorplans as $k => $floor) {
			if($floor->FloorplanName == $current) {
				$prevNext = [];
				if($k > 0 && $k != $total-1) {
					$prevNext['prev'] = $data->floorplans[$k-1];
					$prevNext['next'] = $data->floorplans[$k+1];

					return $prevNext;
				}
				if($k == 0){
					$prevNext['prev'] = '';
					$prevNext['next'] = ( isset( $data->floorplans[$k+1] ) ) ? $data->floorplans[$k+1] : null ;

					return $prevNext;
				}
				if($k == $total-1){
					$prevNext['prev'] = $data->floorplans[$k-1];
					$prevNext['next'] = '';

					return $prevNext;
				}
			}
		}
	}
	return false;
}


function getFloorplan($name, $json) {
	foreach($json as $data){
		foreach ($data->floorplans as $floor) {
			if($floor->FloorplanName == $name) {
				$floor->Category = $data->Name;
				return $floor;
			}
		}
	}
	return false;
}

function getFirst($cat, $json){
	foreach($json as $data) {
		if($data->Name == $cat){
			if ( $data->floorplans[0]->MinimumRent > 1 ) {
				return $data->floorplans[0]->FloorplanName;
			} else {
				return $data->floorplans[1]->FloorplanName;
			}
		}
	}
	return false;
}

function getFloorPlanName($id) {
	switch($id){
		case 0:
			return 'Micro';
			break;
		case 1:
			return '1 Bedroom';
			break;
		case 2:
			return '2 Bedrooms';
			break;
		default:
			return $id;
			break;
	}
}

function getFloorPlanSlug($id) {
	switch($id){
		case 'Micro':
			return 'micro';
			break;
		case '1 Bedroom':
			return '1-bedroom';
			break;
		case '2 Bedrooms':
			return '2-bedroom';
			break;
		default:
			return $id;
			break;
	}
}

function getFloorPlanShort($id) {
	switch($id){
		case 'Micro':
			return 'm';
			break;
		case '1 Bedroom':
			return '1';
			break;
		case '2 Bedrooms':
			return '2';
			break;
		default:
			return $id;
			break;
	}
}

function getFloorPlanOrder($id) {
	switch($id){
		case 'Micro':
			return 0;
			break;
		case '1 Bedroom':
			return 1;
			break;
		case '2 Bedrooms':
			return 2;
			break;
		default:
			return $id;
			break;
	}
}

function getDetails($data){
	$minSqft = 99999999;
	$maxSqft = 0;
	$bestPrice = 99999999;
	$highPrice = 0;

	foreach($data->floorplans as $k => $floorplan) {
		if($floorplan->MinimumRent < $bestPrice) {
			if($floorplan->MinimumRent > 0) {
				$bestPrice = $floorplan->MinimumRent;
			}
		}

		if($floorplan->MaximumRent > $highPrice) {
			$highPrice = $floorplan->MaximumRent;
		}

		foreach($floorplan->Units as $u => $unit) {
			if($unit->SQFT < $minSqft) {
				$minSqft = $unit->SQFT;
			}
			if($unit->SQFT > $maxSqft) {
				$maxSqft = $unit->SQFT;
			}
		}
	}

	$details = (object)[];
	$details->bestPrice = $bestPrice;
	$details->highPrice = $highPrice;
	$details->minSqft = $minSqft;
	$details->maxSqft = $maxSqft;

	return $details;
}

function cmp($a, $b) {
	if ($a->Order == $b->Order) {
		return 0;
	}
	return ($a->Order < $b->Order) ? -1 : 1;
}