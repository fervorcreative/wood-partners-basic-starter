<?php

function my_acf_op_init() {
  // Check function exists.
  if( function_exists('acf_add_options_page') ) {

    // Register options page.
    $option_page = acf_add_options_page(array(
      'page_title'    => __('Site Options'),
      'menu_title'    => __('Site Options'),
      'icon_url'     => 'dashicons-admin-network',
      'menu_slug'     => 'theme-settings',
      'capability'    => 'edit_posts',
      'redirect'      => false
    ));
  }
}
add_action('acf/init', 'my_acf_op_init');