<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$slide_images = get_sub_field('slide_images');
$arrow_color = get_sub_field('arrow_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');

?>

<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  <?php if (!empty($bg_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
  <?php endif; ?>
</style>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'image-slider' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y align-center text-center">
      <div class="cell large-10">
        <div class="alta-slider" data-slick='{"prevArrow" : "<div class=\"slick-prev\"><img src=\"/wp-content/themes/alta-austin-full/assets/images/arrow-prev-<?= $arrow_color; ?>.jpg\" alt=\"Alta Austin Hill\" /></div>", "nextArrow" : "<div class=\"slick-next\"><img src=\"/wp-content/themes/alta-austin-full/assets/images/arrow-next-<?= $arrow_color; ?>.jpg\" alt=\"Alta Austin\" /></div>"}'>
          <?php foreach ($slide_images as $slide): ?>
            <div>
              <img src="<?= $slide['url']; ?>" alt="<?= $slide['alt']; ?>" />
            </div>
          <?php endforeach; ?>
        </div> <!-- .alta-slick-slider --> 
      </div>
    </div>
  </div>
</section>