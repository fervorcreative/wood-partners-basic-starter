<?php 

$section_headline = get_sub_field('section_headline');
$copy_small_title = get_sub_field('copy_small_title');
$copy_large_title = get_sub_field('copy_large_title');
$copy = get_sub_field('copy');
$video_webm = get_sub_field('video_webm');
$video_ogv = get_sub_field('video_ogv');
$video_mp4 = get_sub_field('video_mp4');
$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');

?>

<?php if (!empty($bg_color)): ?>

<style>
  <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
</style>

<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'video-copy' . (!empty($section_classes) ? $section_classes : ''); ?>">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y text-center">
      <div class="cell">
        <h2 class="section-title" data-aos="fade-down" data-aos-duration="600"><?= $section_headline; ?></h2>
        <div class="grid-container grid-x full video-copy-inner">
          <div class="cell large-10" data-aos="fade-right" data-aos-duration="600" data-aos-offset="0" data-aos-easing="ease-in-sine">
            <div class="videoWrapper">
              <video class="embed-responsive-item" autoplay loop muted playsinline>
                <source src="<?= $video_webm; ?>" type="video/webm">
                <source src="<?= $video_ogv; ?>" type="video/ogv">
                <source src="<?= $video_mp4; ?>" type="video/mp4">
                Sorry, your browser doesn't support embedded videos.
              </video>
            </div>
          </div>
          <div class="cell large-4 copy-box" data-aos="fade-left" data-aos-duration="600" data-aos-offset="0" data-aos-easing="ease-in-sine">
            <div class="copy text-left">
              <h6 class="title-small"><?= $copy_small_title; ?></h6>
              <h3 class="title-large"><?= $copy_large_title; ?></h3>
              <p><?= $copy; ?></p>
            </div>
          </div>
        </div>
      </div> 
    </div> 
  </div> 
</section>