<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$hero_type = get_sub_field('image_video');
$video_webm = get_sub_field('video_webm');
$video_ogv = get_sub_field('video_ogv');
$video_mp4 = get_sub_field('video_mp4');
$hero_image = get_sub_field('image');
$tagline = get_sub_field('tagline');
$subheading = get_sub_field('sub_heading');
$live_chat_url = get_sub_field('live_chat_url');
$text_us_url = get_field('text_us_url','option');
 
?>

<?php if (!empty($bg_color) || !empty($hero_image)): ?>
<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  <?php if (!empty($hero_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $hero_image['url']; ?>); }
  <?php endif; ?>
</style>
<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="hero-subpage<?= (!empty($section_classes) ? ' '.$section_classes : ''); ?>">
  <div class="grid-container jumbo-banner-content">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y ">

      <div class="cell home-copy">
        <?php $tag = is_front_page() ? 'h1' : 'h2'; ?>
        <<?= $tag; ?> data-aos="fade-left"><?= $tagline; ?></<?= $tag; ?>>
        <h2 data-aos="fade-right"><?= $subheading; ?></h2>
      </div>
      
    </div>
    <?php if ($live_chat_url): ?>
      <a class="live-chat" href="<?= $live_chat_url; ?>" target="_blank"><span>Live Chat</span></a>
    <?php endif; ?>
    <?php if ($text_us_url): ?>
      <a class="text-us" href="<?= $text_us_url['url']; ?>" target="_blank"><span>Text Us</span></a>
    <?php endif; ?>
  </div>
</section>