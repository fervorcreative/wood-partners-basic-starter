<?php

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$copy = get_sub_field('copy');
$image = get_sub_field('image');
$switch = get_sub_field('switch');
$top_padding = get_sub_field('section_padding_top' );
$bottom_padding = get_sub_field('section_padding_bottom');
$formatted_data = getRentCafe();
$specials = get_field('specials', 'option');
$hide_section = get_sub_field('hide_section');

if (!empty($bg_color)): ?>

<style>
  <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
</style>

<?php endif; ?>

<?php 

// foreach( $specials as $sp ) {
//   $the_special = $sp['the_special'];
//   $special_fp = $sp['floor_plan'];
//   $apply_to_all = $sp['all_floor_plans']; // boolean
// }

if ($hide_section): ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'floorplans' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container">
    <div class="grid-x align-middle">
      <div class="cell small-12 medium-12 large-12 floor-plan-tabs">
        <h2 class="coming-soon">Floor Plan Info Coming Soon</h2>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>

<?php else: ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'floorplans ' . (!empty($section_classes) ? ' '.$section_classes : ''); ?>">
  <div class="grid-container fluid">
    <div class="grid-x align-center">
      <div class="page-content-col cell large-10 floorplan-menu">
          <div class="grid-container">
            <div class="grid-x align-center">
              <div class="cell flex-container">
                <ul class="filter fp-filter">
                    <li><a href="javascript:;" data-filter="*">All Plans</a></li>
                    <?php 
                    $unitName = '';
                    $slugName = '';
                    foreach($formatted_data as $data): 
                      
                      if( $data->Short == 0 ) {
                        $unitName = 'Studio'; 
                        $slugName = 'studio';
                      } elseif($data->Short == 1) {
                        $unitName = '1 Bedroom'; 
                        $slugName = 'one-bedroom';
                      } elseif($data->Short == 2) {
                        $unitName = '2 Bedrooms'; 
                        $slugName = 'two-bedrooms';
                      } elseif($data->Short == 3) {
                        $unitName = '3 Bedrooms'; 
                        $slugName = 'three-bedrooms';
                      }
                      echo '<li><a href="javascript:;" title="'. $unitName .'" data-filter=".'. $slugName .'">'. $unitName .'</a></li>';
                    endforeach; ?>
                    
                </ul>
              </div>
            </div> 
          </div>
  
      </div>

      <!-- Floor Plan menu for mobile -->

      <div class="floorplan-menu-mobile">

        <div class="floorplan-dropdown inline-block relative">
          <button class="button bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
            Select A Room Size
          </button>
          <ul class="floorplan-dropdown-menu absolute text-gray-700 pt-1">
          <li><a href="javascript:;" data-filter="*">All Plans</a></li>
                <?php 
                $unitName = '';
                $slugName = '';
                foreach($formatted_data as $data): 
                  
                  if( $data->Short == 0 ) {
                    $unitName = 'Studio'; 
                    $slugName = 'studio';
                  } elseif($data->Short == 1) {
                    $unitName = '1 Bedroom'; 
                    $slugName = 'one-bedroom';
                  } elseif($data->Short == 2) {
                    $unitName = '2 Bedrooms'; 
                    $slugName = 'two-bedrooms';
                  } elseif($data->Short == 3) {
                    $unitName = '3 Bedrooms'; 
                    $slugName = 'three-bedrooms';
                  }
                  echo '<li><a href="javascript:;" title="'. $unitName .'" data-filter=".'. $slugName .'" data-filter=".'. $slugName .'">'. $unitName .'</a></li>';
                endforeach; ?>

          </ul>
        </div>

      </div>
      <!-- END  -->  




      <div class="page-content-col plans-table cell large-10">
        <div class="section">
          <div class="grid-container fluid">
            <div class="grid-x grid-margin-x">
              
              <div class="cell small-12">
                  <?php //var_dump( $specials ); ?>
                <div id="floor-plans" class="floor-plan-units">
                  <?php 
                  foreach( $formatted_data as $data ) : 
                    
                    if( $data->Short == 0 ) {
                      $unitName = 'Studio'; 
                      $slugName = 'studio';
                      $order = 0;
                    } elseif($data->Short == 1) {
                      $unitName = '1 Bedroom'; 
                      $slugName = 'one-bedroom';
                      $order = 1;
                    } elseif($data->Short == 2) {
                      $unitName = '2 Bedrooms'; 
                      $slugName = 'two-bedrooms';
                      $order = 2;
                    } elseif($data->Short == 3) {
                      $unitName = '3 Bedrooms'; 
                      $slugName = 'three-bedrooms';
                      $order = 3;
                    }
                    //echo '<p>There is a special for ' . $special_fp . '</p>';
                    foreach($data->floorplans as $floorplan) : ?>
                      <div class="fp-wrapper mix <?= $slugName; ?>" data-unitname="<?= $slugName; ?>" data-order="<?= $order; ?>" data-sqft="<?= $floorplan->MinimumSQFT; ?>">
                        <div class="fp-details align-middle">
                          <?php
                          $name = $floorplan->FloorplanName; 
                          $fp_name_slug = strtolower( str_replace(array('.', '_', ' '), '', $name ) );
                          $baths = $floorplan->Baths;
                          $minRent = $floorplan->MinimumRent;
                          $sqFeet = $floorplan->MinimumSQFT;
                          $beds = $floorplan->Beds; 
                          $floorplan_url = $floorplan->AvailabilityURL;
                          $floorplan_img_url = $floorplan->FloorplanImageURL;
                          $unitsAvailable = $floorplan->AvailableUnitsCount;
                          $has_specials = $floorplan->FloorplanHasSpecials;
                          /*
                           * According to Yardi/RentCafe's API docs: 
                           * -1: Floor plan is associated with specials.
                           * 0: Floor plan is not associated with specials.
                           */
                          if ($floorplan_url == ''):
                            $floorplan_url = 'https://www.on-site.com/apply/property/393254?floorplan='.$name;
                          endif;
                          // for testing
                          // $testing_fp = array('EA1', '1A', '2A');
                          // if ( in_array($name, $testing_fp)  ) {
                          //   $has_specials = -1;
                          //   $unitsAvailable = 2;
                          // }
                          
                          ?>
                          
                          <div class="fp-name"><h4><?php echo $name; ?></h4></div>
                          <div class="fp-bath"><p><?php echo $baths . ' BATH'; ?></p></div>
                          <div class="fp-room"><p><?php echo $sqFeet; ?> SQFT</p></div>
                          <div class="fp-price"><p>Starting at: $<?php echo $minRent; ?></p></div>
                          <?php if (($unitsAvailable > 1 && $has_specials === -1 ) && ($apply_to_all || $special_fp === $name)) {
                            echo '<div class="fp-specials"><p>Special Available</p></div>';
                          } else {
                            echo '<div class="fp-specials">&nbsp;</div>';
                          } ?>
                          
                          <div class="fp-btn"><a href="#floor-plan-<?php echo $fp_name_slug; ?>" class="button" data="view-plan">View Floor Plan</a></div>
                        </div><!-- end fp-details -->
                        
                        <div id="floor-plan-<?php echo $fp_name_slug; ?>" class="fp-fullview">
                          <div class="fp-fullview--name">
                            <h3><?php echo $name; ?></h3>
                            <p><?php echo $unitName; ?></p>
                            <?php 
                            
                            if( $unitsAvailable > 5 ) {
                              // all units above 5
                              if( $has_specials === -1 && ($special_fp === $name || $apply_to_all) ) {
                                echo '<div class="callout"><p>Current Special - '. $the_special . '</p></div>';
                              }
                              //echo '<p><a href="' . esc_url($floorplan_url) . '" target="_blank" class="button button--thin">Apply Now</a></p>';
                              
                            } elseif( $unitsAvailable > 0 && $unitsAvailable <= 5 ) {

                              
                              // all units below 5
                              echo '<div class="callout">';
                              echo '<p>Hurry! This floor plan is selling fast, only ' . $unitsAvailable . ' left!</p>';
                              echo '<p>Current Special – One Month Free!</p>';
                              if ($has_specials === -1 && ($apply_to_all || $special_fp === $name)  ) {
                                // echo '<p>Current Special - One Month Free!</p>';
                                echo '<p>Current Special - '. $the_special . '</p>';
                              }
                              echo '</div> <!-- end callout -->';
                              echo '<button><a href="' . esc_url($floorplan_url) . '" target="_blank" class="button button--thin">Apply Now</a></button>';
                            } else {
                              // unavailable units
                              echo '<p>Floor Plan <em>'. $name .'</em> is not available</p>';
                            }
                            
                            
                            
                            
                            // if ($unitsAvailable <= 1) {
                            //   echo '<p>Floor Plan <em>'. $name .'</em> is not available</p>';
                            // } elseif ($unitsAvailable <= 5) {
                            //   echo '<div class="callout">';
                            //   echo '<h5>Hurry! This floor plan is selling fast, only ' . $unitsAvailable . ' left!</h5>';
                            //   if ($has_specials === -1 || $apply_to_all || $special_fp === $name  ) {
                            //     //echo '<p>Current Special - One Month Free!</p>';
                            //     echo '<p>Current Special - '. $the_special . '</p>';
                            //   }
                            //   echo '</div> <!-- end callout -->';
                            // } else {
                            //   // all units that available
                            //   if( $has_specials === -1 && $special_fp === $name || $apply_to_all ) {
                            //     echo '<div class="callout"><p>Current Special - '. $the_special . '</p></div>';
                            //   }
                            // }
                            
                            ?>
                            
                          </div><!-- end fp-fullview--name -->
                          <div class="fp-fullview--image">
                            <a href="<?php echo esc_url($floorplan_img_url); ?>" class="mfp-image">
                              <!-- <img src="<?php echo esc_url($floorplan_img_url); ?>" alt="Floorplan Image"> -->
                              <img src="<?= get_template_directory_uri() . '/assets/images/fp-placeholder.png'; ?>" alt="Alta Federal Hill Floorplan" />
                            </a>
                          </div><!-- .fp-fullview--image -->
                        </div><!-- end fp-fullview -->
                      </div>
                    <?php endforeach; ?>
                  <?php endforeach; ?>
                </div><!-- end .floor-plans -->
          
              </div>
            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</section>

<?php endif; ?>