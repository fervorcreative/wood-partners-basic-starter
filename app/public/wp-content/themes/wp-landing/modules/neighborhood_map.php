<?php

$bg_color = get_sub_field('bg_color');
$locations = get_locations();
$map_section_title = get_sub_field('map_section_title');
$categories = get_terms(['taxonomy' => 'wpseo_locations_category']);

?>

<style>
  #neighborhood-map { background-color: <?= $bg_color; ?>; }
</style>

<section id="neighborhood-map" class="section neighborhood-map">
    <div class="grid-container">
        <div class="grid-x align-center text-center">
            <div class="cell medium-10 filters-holder" data-aos="fade-up">
              <h1 class="section-title"><?= $map_section_title?></h1>
            </div>

            <div class="cell medium-10 filters-holder map-menu" data-aos="fade-up">
                <ul class="filters">
                    <li>
                        <button class="active" data-filter="*">All</button>
                    </li>
                    <?php foreach ($categories as $term) : ?>
                        <li>
                            <button data-filter="<?php echo $term->name; ?>"><?php echo $term->name; ?></button>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <!-- Floor Plan menu for mobile -->

            <div class="map-menu-mobile">

                <div class="map-dropdown inline-block relative">
                    <button class="button bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
                        Select Category
                    </button>
                    <ul class="map-dropdown-menu absolute text-gray-700 pt-1">
                        <li>
                            <a class="active" data-filter="*" >All</a>
                        </li>
                        <?php foreach ($categories as $term):  ?>
                        <li>
                            <a data-filter="<?= $term->name; ?>" ><?= $term->name; ?></a>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>
            <!-- END  -->  
            <div class="cell medium-10 map-holder" data-aos="fade-up">
                <div class="map"></div>
            </div>
            
            <div class="cell medium-10 large-10 location-features-section" data-aos="fade-up">
                <ul class="locations features">
                <?php foreach ($locations as $location):
                  //print_r($location);
                  $terms = get_the_terms($post, 'location_categories');
                  $term = !empty($terms) ? reset($terms) : [];
                  $address1 = $location->_wpseo_business_address;
                  $city = $location->_wpseo_business_city; 
                  $state = $location->_wpseo_business_state;
                  $zip = $location->_wpseo_business_zipcode; 

                  //print_r($location); ?>

                  <li data-lat="<?= $location->_wpseo_coordinates_lat; ?>" data-lng="<?= $location->_wpseo_coordinates_long; ?>" data-marker="<?php printf('%s/assets/images/map-markers/marker-%s.svg', get_template_directory_uri(), $location->category->slug); ?>" data-category="<?= $location->category->name; ?>" data-name="<?= $location->post_title ?>" data-address="<?= $address1. '<br/>' .$city.',' .$state .' '. $zip; ?>">
                  <?= $location->post_title; ?>
                  </li>
                <?php endforeach; ?>
                
                </ul>
            </div>
        </div>
    </div>
</section>