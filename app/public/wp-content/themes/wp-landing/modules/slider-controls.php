<?php if (is_page('gallery')): ?>

<div class="slider-controls">
  <button class="flickity-button flickity-prev-next-button slick-arrow slick-prev previous custom"><img src="/wp-content/themes/alta-leander/assets/images/gallery-slider-arrow-left.jpg" alt="Alta Leander Station" /></button>
  
  <button class="flickity-button flickity-prev-next-button slick-arrow slick-next next custom"><img src="/wp-content/themes/alta-leander/assets/images/gallery-slider-arrow-right.jpg" alt="Alta Leander Station" /></button>
</div>

<?php endif; ?>