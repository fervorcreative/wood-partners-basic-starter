<?php

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$section_headline = get_sub_field('section_headline');
$section_copy = get_sub_field('section_copy');
$bg_color = get_sub_field('bg_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$images = get_sub_field('images');
$content = get_sub_field('content');
$show_decor = get_sub_field('show_decor');
$categories = array_unique(array_flatten(wp_list_pluck($images, 'categories')));
sort($categories);

$section_headline_image_overlay = get_sub_field('section_headline_image_overlay');

?>

<style>
  section.virtual-tours { background-color: <?= $bg_color; ?>; }
</style>



<section class="virtual-tours">
    <?php if ($show_decor): ?>
        <div class="decor1" data-aos="fade-down" data-aos-duration="600"></div>
        <div class="decor2" data-aos="fade-down" data-aos-duration="600"></div>
    <?php endif; ?>
    <!-- <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="call medium-8">
                <div class="virtual-tours-header">
                    <img class="virtual-tours-title_overlay"src="<?= $section_headline_image_overlay['url']; ?>" />
                    <h2 class="virtual-tours-title"><?= $section_headline; ?></h2>
                </div>
            </div>
            <div class="call medium-4">

            </div>
        </div>
    </div> -->
    <div class="grid-container full">
        <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y align-center text-center">
            <div class="cell medium-10 large-10">
                <div class="virtual-tours-header" data-aos="zoom-in">
                    <img class="virtual-tours-title_overlay"src="<?= $section_headline_image_overlay['url']; ?>" />
                    <h2 class="virtual-tours-title"><?= $section_headline; ?></h2>
                </div>

                <!-- <h2><?= $section_headline; ?></h2>
                <p><?= $section_copy; ?></h2>
                
                <ul class="filters">
                    <li>
                        <button class="active" data-filter="*">All</button>
                    </li>
                    <?php foreach ($categories as $term) : ?>
                        <li>
                            <button data-filter="<?php echo $term; ?>"><?php echo $term; ?></button>
                        </li>
                    <?php endforeach; ?>
                </ul> -->

                <div class="grid-x grid-margin-x virtual-tour-grid">
                    <?php foreach ($images as $image) : ?>
                        <div class="cell small-12 medium-12 large-6" data-filter="<?= $image['categories'][0]; ?>">
                            
                            <?php if ($image['link']) : ?>
                                <a class="mfp-video" href="<?php echo $image['link']['url']; ?>" target="<?php echo $image['link']['target']; ?>" rel="<?php echo $image['link']['target'] == '_blank' ? 'noopener noreferrer' : ''; ?>">
                                <img class="tour-image" src="<?= $image['image']['url']; ?>" alt="Alta River Oaks <?= $image['categories'][0]; ?>"/>
                                </a>
                            <?php else : ?>
                                <button>
                                <img class="tour" src="<?= $image['image']['url']; ?>" alt="Alta River Oaks <?= $image['categories'][0]; ?>"/>
                                </button>
                            <?php endif; ?>
                            <div class="tour-title">
                                <p class="virtual-tour-indv-title"><?= $image['title']; ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>