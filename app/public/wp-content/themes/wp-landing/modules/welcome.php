<?php 

$copy_large_title = get_sub_field('copy_large_title');
$copy_small_title = get_sub_field('copy_small_title');
$copy = get_sub_field('copy');
$decor = get_sub_field('decor');
$image = get_sub_field('image');
$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_image = get_sub_field('bg_image');
$bg_color = get_sub_field('bg_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$switch = get_sub_field('switch');

?>

<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> .welcome-inner { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  <?php if (!empty($bg_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
  <?php endif; ?>

</style>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'welcome' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?= (!empty($switch) ? ' switch' : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container" data-aos="fade-up" data-aos-duration="500">
    <div class="grid-x">
      <div class="cell medium-12 large-12 welcome-wrapper">
        <div class="grid-container grid-x full align-middle welcome-inner<?= (!empty($switch) ? ' align-right' : ''); ?>">
          <?php if (!empty($decor)): ?>
            <img class="decor" src="<?= $decor['url']; ?>" alt="<?= $decor['alt']; ?>" />
          <?php endif; ?>
          <div class="cell medium-12 large-4 xlarge-4">
            <div class="imageWrapper">
              <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" />
            </div>
          </div>
          <div class="cell medium-12 large-8 xlarge-8 copy-box">
            <div class="copy text-left">
              <h3 class="title-large"><?= $copy_large_title; ?></h3>
              <?= $copy; ?>
            </div>
          </div>
        </div>
      </div> 
    </div> 
  </div> 
</section>