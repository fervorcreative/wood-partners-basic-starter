<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$copy_title = get_sub_field('copy_title');
$copy = get_sub_field('copy');
$image = get_sub_field('image');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$switch = get_sub_field('switch');
$top_padding = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' );

?>

<?php if (!empty($bg_color) || !empty($bg_image)): ?>
  <style>
    <?php if (!empty($bg_color)): ?>
      <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
    <?php endif; ?>

    <?php if (!empty($bg_image)): ?>
      <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
    <?php endif; ?>
  </style>
<?php endif; ?>


<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'image-copy-subpage' . (!empty($section_classes) ? ' '.$section_classes : ''); ?> <?= (!empty($switch) ? 'switch' : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">

    <div class="grid-container ">
      <div class="grid-x">
        <div class="cell large-6 image-copy-subpage-image" data-aos="fade-right" data-aos-duration="600">
          <div class="accent">
            <?= wp_get_attachment_image( $image['ID'], 'full'); ?>
          </div>
        </div>
        <div class="cell large-6 image-copy-subpage-copy" data-aos="fade-left" data-aos-duration="600">
          <div class="copy">
            <h2><?= $copy_title; ?></h2>
            <?= $copy; ?>
          </div>
        </div>
      </div>
    </div>

</section>