<?php 

$copy_large_title = get_sub_field('copy_large_title');
$copy_small_title = get_sub_field('copy_small_title');
$copy = get_sub_field('copy');
$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$decor = get_sub_field('decor');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$heading_font_color = get_sub_field('heading_font_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$switch = get_sub_field('switch');

?>

<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  /* <?php if (!empty($bg_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
  <?php endif; ?> */

  <?php if (!empty($heading_font_color)): ?>
    <?= '#'.$section_id; ?> h2 { color: <?= $heading_font_color; ?>; }
  <?php endif; ?>
</style>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'image-copy-home' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?= (!empty($switch) ? ' switch' : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell small-12 medium-8 large-8 home-image-left">
        <div class="imageWrapper">
          <img src="<?= $bg_image['url']; ?>" alt="<?= $bg_image['alt']; ?>" />
        </div>
      </div>
      <div class="cell small-12 medium-4 large-4 home-copy-right">
        <h2><?= $copy_large_title; ?></h2>
        <div class="copy">
          <?= $copy; ?>
        </div>
      </div>
    </div>
  </div>
</section>

