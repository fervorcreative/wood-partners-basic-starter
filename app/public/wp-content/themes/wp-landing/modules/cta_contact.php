<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding'); 

$decorative_image_left = get_sub_field('decorative_image_left');
$decorative_image_right = get_sub_field('decorative_image_right'); 

$contact_info = get_sub_field('contact_info'); 

$show_schedule_tour = get_sub_field('show_schedule_tour');
$schedule_tour_url = get_sub_field('schedule_tour_url');  
$show_apply_now = get_sub_field('show_apply_now'); 
$apply_now_url = get_sub_field('apply_now_url');

$show_text_us = get_sub_field('show_text_us'); 
$text_us_url = get_sub_field('text_us_url'); 

$show_live_chat = get_sub_field('show_live_chat'); 
$live_chat_url = get_sub_field('live_chat_url');

$show_facebook = get_sub_field('show_facebook'); 
$facebook_icon = get_sub_field('facebook_icon'); 
$facebook_url = get_sub_field('facebook_url'); 

$show_instagram = get_sub_field('show_instagram'); 
$instagram_icon = get_sub_field('instagram_icon');
$instagram_url = get_sub_field('instagram_url');

?>

<?php if (!empty($bg_color) || !empty($bg_image)): ?>
<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  <?php if (!empty($bg_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
  <?php endif; ?>
</style>
<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'cta-contact' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?= (!empty($offset) ? 'offset' : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y">
      <div class="cell large-4 cta-logo" data-aos="fade-up">
        <?php if ($decorative_image_left): ?>
          <div class="decor-left"><?= wp_get_attachment_image( $decorative_image_left['ID'], 'full'); ?></div>
        <?php endif; ?>
      </div>

      <div class="cell large-4 cta-contact-info" data-aos="fade-up">
        <?= $contact_info; ?>
        <div class="contact-info-buttons">
          <?php if ($show_apply_now): ?>
            <a class="gf-button button apply-now" href="<?= $apply_now_url ?>" target="_blank">APPLY NOW</a>
          <?php endif; ?>
          <?php if ($show_schedule_tour): ?>
            <a class="gf-button button schedule-tour" href="<?= $schedule_tour_url ?>" target="_blank">SCHEDULE A TOUR</a>
          <?php endif; ?>
        </div>
      </div>

      <div class="cell large-4 cta-links" data-aos="fade-up">

        <?php if ($show_text_us): ?>
          <a class="text-us" href="<?= $text_us['url']; ?>" target="_blank">Text Us</a>
        <?php endif; ?>

        <?php if ($show_live_chat): ?>
          <a class="live-chat" href="<?= $live_chat['url']; ?>" target="_blank">Live Chat</a>
        <?php endif; ?>

        <?php if ($show_facebook): ?>
          <a class="facebook" href="<?= $facebook_url; ?>" target="_blank"><img src="<?= $facebook_icon['url']; ?>" alt="" /></a>
        <?php endif; ?>

        <?php if ($show_instagram): ?>
          <a class="instagram" href="<?= $instagram_url; ?>" target="_blank"><img src="<?= $instagram_icon['url']; ?>" alt="" /></a>
        <?php endif; ?>
      </div>

      <?php if ($decorative_image_right): ?>
        <div class="decor-right"><?= wp_get_attachment_image( $decorative_image_right['ID'], 'full'); ?></div>
      <?php endif; ?>


    </div>
  </div> 
</section>