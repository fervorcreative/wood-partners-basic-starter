<?php 

$section_headline = get_sub_field('section_headline');
$copy_small_title = get_sub_field('copy_small_title');
$copy_large_title = get_sub_field('copy_large_title');
$copy = get_sub_field('copy');
$video_webm = get_sub_field('video_webm');
$video_ogv = get_sub_field('video_ogv');
$video_mp4 = get_sub_field('video_mp4');
$video_type = get_sub_field('video_type');
$video_youtube_url = get_sub_field('video_youtube_url');
$video_placeholder = get_sub_field('video_placeholder');
$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');

?>

<?php if (!empty($bg_color)): ?>

<style>
  <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
</style>

<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'video-single' . (!empty($section_classes) ? ' '.$section_classes : ''); ?>"">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y align-center text-center">
      <div class="cell medium-10 large-9" data-aos="fade-down" data-aos-duration="600">
        <div class="videoWrapper">
          <video <?= (!empty($video_placeholder) ? ' poster="'.$video_placeholder['url'].'"' : ''); ?> class="embed-responsive-item" controls loop playsinline>
            <source src="<?= $video_webm; ?>" type="video/webm">
            <source src="<?= $video_ogv; ?>" type="video/ogv">
            <source src="<?= $video_mp4; ?>" type="video/mp4">
            Sorry, your browser doesn't support embedded videos.
          </video>
        </div>
      </div> 
    </div>
  </div> 
</section>