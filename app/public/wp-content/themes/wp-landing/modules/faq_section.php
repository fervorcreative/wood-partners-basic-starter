<?php 

$title = get_sub_field('title');
$top_image_spacer = get_sub_field('top_image_spacer');

$faqs = get_sub_field('faqs');

$categories = array_unique(array_flatten(wp_list_pluck($faqs, 'category')));

?>


<?php



?>

<section class="full-faq">

  

  <div class="grid-container faq-menu">
    <div class="grid-x"> 
      <div class="cell large-12">
        <ul class="faq-filters">
          <?php foreach ($categories as $term):  ?>
            <li>
              <button class="menu-option"type="submit" data-filter=".<?= $term?>" data-sort=".<?= $term ?>" ><?= $term ?></button>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>

  <!-- Floor Plan menu for mobile -->

  <div class="faq-menu-mobile">

    <div class="faq-dropdown inline-block relative">
      <button class="button bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
        Select Category
      </button>
      <ul class="faq-dropdown-menu absolute text-gray-700 pt-1">
        <?php foreach ($categories as $term):  ?>
          <li>
            <a data-filter=".<?= $term?>" data-sort=".<?= $term ?>" ><?= $term ?></a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>

  </div>
  <!-- END  -->   

  <div class="grid-container">
    <div class="grid-x"> 
      <div class="cell large-12"></div>
        <div class="faq-list" data-accordion>

          <?php foreach ($faqs as $faq): $category = $faq['category'][0]; ?>
            <div class="faq-wrapper mix <?= $category; ?>" >
              <div class="faq-item" value="<?= $category ?>" >
                <a class="faq-title"><?= $faq['question'] ?></a>
                <p><?= $faq['answer'] ?></p>
              </div>
            </div>
          <?php endforeach; ?>

        </div>
      </div>
    </div>
  </div>

  <div class="grid-container full">
    <div class="grid-x bottom-faq-background-spacer">
      <div class="cell large-12  ">
        <img src="<?=$top_image_spacer['url'] ?>" alt="<?= $top_image_spacer['alt']; ?>"/>
      </div>
    </div>
  </div>

</section>

