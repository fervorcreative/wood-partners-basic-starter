<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$headline = get_sub_field('section_headline');
$headline_color = get_sub_field('section_headline_color');
$show_decor = get_sub_field('show_decor');
$arrow_color = get_sub_field('arrow_color');
$show_button = get_sub_field('show_button');
$button = get_sub_field('button');
$slide_images = get_sub_field('slide_images');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');

?>

<?php if (!empty($bg_color)): ?>

<style>
  <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?= '#'.$section_id; ?> h2 { color: <?= $headline_color; ?>; }
</style>

<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'image-slider-subpage' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y align-center text-center">
      <div class="cell">
        <?php if ($show_decor): ?>
          <div class="decor1"></div>
          <div class="decor2"></div>
        <?php endif; ?>
        <?php if ($headline): ?>
          <h2 class="slider-title"><?= $headline; ?></h2>
        <?php endif; ?>
        <div class="alta-slider" data-slick='{"prevArrow" : "<div class=\"slick-prev\"><img src=\"/wp-content/themes/fervor-altafh-full/assets/images/arrow-prev-<?= $arrow_color; ?>.svg\" alt=\"Alta Federal Hill\" /></div>", "nextArrow" : "<div class=\"slick-next\"><img src=\"/wp-content/themes/fervor-altafh-full/assets/images/arrow-next-<?= $arrow_color; ?>.svg\" alt=\"Alta Federal Hill\" /></div>"}'>
          <?php foreach ($slide_images as $slide): ?>
            <div>
              <img src="<?= $slide['url']; ?>" alt="<?= $slide['alt']; ?>" />
            </div>
          <?php endforeach; ?>
        </div> <!-- .alta-slick-slider --> 
        <?php if ($show_button): ?>
          <a href="<?= $button['url']; ?>" class="button white"><?= $button['title']; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>