<?php

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$bg_color = get_sub_field('bg_color');
$images = get_sub_field('images');
$title = get_sub_field('title');
$copy = get_sub_field('copy');
$show_decor = get_sub_field('show_decor');
$categories = array_unique(array_flatten(wp_list_pluck($images, 'categories')));
sort($categories);

?>

<?php if (!empty($bg_color)): ?>

<style>
  <?= '#'.$section_id .'{ background-color:'.$bg_color.'; } #gallery-grid { background-color:'.$bg_color.'; }'; ?>
</style>

<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'gallery-slider' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?php padding_top_classes(); ?>">

<?php if ($show_decor): ?>
  <div class="decor1" data-aos="fade-down" data-aos-duration="600"></div>
  <div class="decor2" data-aos="fade-down" data-aos-duration="600"></div>
<?php endif; ?>

  <div class="grid-container fluid">
    <div class="grid-x align-center align-middle">
      <div class="cell small-12 medium-10 large-10">

        <div class="gallery-intro">
          <h2 class="gallery-title"><?= $title; ?></h2>
          <?= $copy; ?>
        </div> <!-- .gallery-intro -->

        <div class="slider-container">

          <div class="slider">
            <?php foreach ($images as $image): ?>
              <div class="slide" data-filter="<?= $image['categories'][0]; ?>">
                <img src="<?= $image['image']['url']; ?>" alt="Alta Leander Station <?= $image['categories'][0]; ?>"/>
              </div>
            <?php endforeach; ?>
          </div>

          <div class="controls">
              <?php get_template_part('modules/slider-controls') ?>
          </div>

        </div> <!-- .slider-container -->

      </div>
    </div>
  </div>
</section>

<section id="gallery-grid" class="gallery<?php padding_bottom_classes(); ?>">

  <?php if ($show_decor): ?>
    <div class="decor1"></div>
  <?php endif; ?>

  <div class="grid-container fluid">
    <div class="grid-x align-center">
      <div class="cell medium-10 gallery-items-holder">

        <ul class="filters gallery-menu">
          <li>
            <a class="top-links" class="active" data-filter="*">All</a>
          </li>
          <?php foreach ($categories as $term) : ?>
            <li>
              <a class="top-links" data-filter="<?php echo $term; ?>"><?php echo $term; ?></a>
            </li>
          <?php endforeach; ?>
        </ul>

        <!-- Floor Plan menu for mobile -->

        <div class="gallery-menu-mobile">

          <div class="gallery-dropdown inline-block relative">
            <button class="button bg-gray-300 text-gray-700 font-semibold py-2 px-4 rounded inline-flex items-center">
              Select Category
            </button>
            <ul class="gallery-dropdown-menu filters absolute text-gray-700 pt-1">
              <li>
                <a data-filter="*">All</a>
              </li>
              <?php foreach ($categories as $term):  ?>
                <li>
                  <a data-filter="<?= $term?>" data-sort="<?= $term ?>"><?= $term ?></a>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>

        </div>
        <!-- END  -->

        <div class="grid-x grid-margin-x gallery-grid">
          <?php foreach ($images as $image) : ?>
            <div class="cell small-12 medium-12 large-6" data-filter="<?= $image['categories'][0]; ?>">
              <?php $src = wp_get_attachment_image_src($image['image'], 'large'); ?>
              <?php if ($image['link']) : ?>
                <a href="<?php echo $image['link']['url']; ?>" target="<?php echo $image['link']['target']; ?>" rel="<?php echo $image['link']['target'] == '_blank' ? 'noopener noreferrer' : ''; ?>">
                  <img src="<?= $image['image']['url']; ?>" alt="Alta River Oaks <?= $image['categories'][0]; ?>"/>
                </a>
              <?php else : ?>
                <button>
                  <img src="<?= $image['image']['url']; ?>" alt="Alta River Oaks <?= $image['categories'][0]; ?>"/>
                </button>
              <?php endif; ?>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>
