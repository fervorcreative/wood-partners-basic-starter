<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$headline = get_sub_field('headline');
$watermark = get_sub_field('watermark_image');
$copy = get_sub_field('copy');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$copy = get_sub_field('copy');

?>

<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  <?php if (!empty($bg_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
  <?php endif; ?>
</style>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?>class="<?= 'contact-form-cta' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y align-center align-middle" data-aos="fade-up">
      <div class="cell large-10 copy bg-white">
        <h2 class="text-center"><?= $headline; ?></h2>
        <?= $copy; ?>
      </div>
    </div>
  </div>
  <div id="watermark"><?= wp_get_attachment_image($watermark['ID'], 'full' ); ?></div>
</section>