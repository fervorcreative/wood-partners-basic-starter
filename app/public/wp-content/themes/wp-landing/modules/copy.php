<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$copy = get_sub_field('copy');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$top_padding = get_sub_field( 'section_padding_top' );
$bottom_padding = get_sub_field( 'section_padding_bottom' );

?>

<?php if (!empty($bg_color) || !empty($bg_image)): ?>
  <style>
    <?php if (!empty($bg_color)): ?>
      <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
    <?php endif; ?>

    <?php if (!empty($bg_image)): ?>
      <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
    <?php endif; ?>
  </style>
<?php endif; ?>


<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'copy-subpage' . (!empty($section_classes) ? ' '.$section_classes : ''); ?> <?= (!empty($switch) ? 'switch' : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">

    <div class="grid-container ">
      <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y">
        <div class="cell large-3 copy-extra-space" data-aos="fade-left" data-aos-duration="600">
        </div>
        <div class="cell small-12 large-6" data-aos="fade-left" data-aos-duration="600">
          <div class="orange-border-top"></div>
          <div class="copy-subpage-text">
            <?= $copy; ?>
          </div>
        </div>
        <div class="cell large-3 copy-extra-space" data-aos="fade-left" data-aos-duration="600">
        </div>
      </div>
    </div>

</section>