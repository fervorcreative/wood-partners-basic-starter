<?php

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$section_headline = get_sub_field('section_headline');
$copy_small_title = get_sub_field('copy_small_title');
$copy_large_title = get_sub_field('copy_large_title');
$apply_top_negative_margin = get_sub_field('apply_top_negative_margin');
$top_negative_margin = get_sub_field('negative_margin');
$copy = get_sub_field('copy');
$image = get_sub_field('image');
$switch = get_sub_field('switch');
$offset = get_sub_field('apply_vertical_offset');

?>

<?php if (!empty($bg_color) || !empty($bg_image) || $apply_top_negative_margin==true): ?>
<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  <?php if (!empty($bg_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
  <?php endif; ?>

  <?php if ($apply_top_negative_margin): ?>
    <?= '#'.$section_id; ?> .image-copy-inner { margin-top: <?= $top_negative_margin; ?>; }
  <?php endif; ?>
</style>
<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'image-copy' . (!empty($section_classes) ? ' '.$section_classes : ''); ?> <?= (!empty($switch) ? 'switch' : ''); ?> <?= (!empty($offset) ? 'offset' : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y text-center">
      <div class="cell">
        <?php if ($section_headline): ?>
          <h2 class="section-title" data-aos="fade-down" data-aos-duration="600"><?= $section_headline; ?></h2>
        <?php endif; ?>
        <div class="grid-container grid-x full <?= (is_page('contact-us') ? 'map-copy-inner' : 'image-copy-inner'); ?> <?= (!empty($switch) ? 'align-right' : ''); ?>">
          <div class="cell large-6 image" data-aos="fade-up" data-aos-duration="600" data-aos-offset="0" data-aos-easing="ease-in-sine">
            <?php if (is_page('contact-us')): ?>
              <div id="map" style="width:100%;height:720px"></div>
            <?php else: ?>
              <div class="imageWrapper">
                <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" />
              </div>
            <?php endif; ?>
          </div>

          <div class="cell large-6 copy-box" data-aos="fade-up" data-aos-duration="600" data-aos-offset="0" data-aos-easing="ease-in-sine">
            <div class="copy text-left flex-container flex-dir-column">
              <div class="copy-box-copy">
                <h3 class="title-large"><?= $copy_large_title; ?></h3>
                <?= $copy; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
