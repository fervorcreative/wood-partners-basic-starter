<?php 

$headline = get_sub_field('headline');
$copy = get_sub_field('copy');
$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');

?>

<?php if (!empty($bg_color)): ?>

<style>
  <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
</style>

<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'headline-copy' . (!empty($section_classes) ? ' '.$section_classes : ''); ?>">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y">
      <div class="cell medium-8" data-aos="fade-down" data-aos-duration="600">
        <h2><?= $headline; ?></h2>
      </div>
      <div class="cell medium-offset-3 medium-8" data-aos="fade-down" data-aos-duration="600">
        <?= $copy; ?>
      </div>
    </div>
  </div>
</section>