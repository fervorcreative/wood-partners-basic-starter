<?php 

$copy_large_title_left = get_sub_field('copy_large_title_left');
$copy_small_title_left = get_sub_field('copy_small_title_left');
$copy_large_title_right = get_sub_field('copy_large_title_right');
$copy_small_title_right = get_sub_field('copy_small_title_right');
$image_left = get_sub_field('image_left');
$image_right = get_sub_field('image_right');
$copy_left = get_sub_field('copy_left');
$copy_right = get_sub_field('copy_right');
$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');

?>

<style>
  <?php if (!empty($bg_color)): ?>
    <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
  <?php endif; ?>

  <?php if (!empty($bg_image)): ?>
    <?= '#'.$section_id; ?> { background-image: url(<?= $bg_image['url']; ?>); }
  <?php endif; ?>
</style>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?> class="<?= 'image-copy-two-column' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y align-center">
      <div class="cell xlarge-6 box-wrapper" data-aos="fade-right">
        <div class="box left">
          <h3><?= $copy_large_title_left; ?></h3>
          <?= $copy_left; ?>
        </div>
        <img src="<?= $image_left['url']; ?>" alt="<?= $image_left['alt']; ?>" />
       </div>

       <div class="cell xlarge-6 box-wrapper" data-aos="fade-left">
        <div class="box right">
          <h3><?= $copy_large_title_right; ?></h3>
          <?= $copy_right; ?>
        </div>
        <img src="<?= $image_right['url']; ?>" alt="<?= $image_right['alt']; ?>" />
      </div>
    </div>
  </div>
</section>