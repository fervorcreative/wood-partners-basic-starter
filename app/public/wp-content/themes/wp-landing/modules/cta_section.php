<?php 

$section_id = get_sub_field('section_id');
$section_classes = get_sub_field('section_classes');
$bg_color = get_sub_field('bg_color');
$bg_image = get_sub_field('bg_image');
$headline = get_sub_field('headline');
$watermark = get_sub_field('watermark_image');
$copy = get_sub_field('copy');
$padding_top = get_sub_field('section_top_padding');
$padding_bottom = get_sub_field('section_bottom_padding');
$copy = get_sub_field('copy');

?>

<?php if (!empty($bg_color)): ?>

<style>
  <?= '#'.$section_id; ?> { background-color: <?= $bg_color; ?>; }
</style>

<?php endif; ?>

<section <?= (!empty($section_id) ? 'id="'.$section_id.'"' : ''); ?>class="<?= 'contact-cta' . (!empty($section_classes) ? ' '.$section_classes : ''); ?><?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container">
    <div class="grid-x grid-padding-x grid-padding-y grid-margin-x grid-margin-y align-middle">
      <div class="cell large-6 copy">
        <?= wp_get_attachment_image( $bg_image['ID'], 'full' ); ?>
        <h2><?= $headline; ?></h2>
      </div>
      <div class="cell large-4">
        <?= $copy; ?>
      </div>
    </div>
  </div>
  <div id="watermark"><?= wp_get_attachment_image($watermark['ID'], 'full' ); ?></div>
</section>