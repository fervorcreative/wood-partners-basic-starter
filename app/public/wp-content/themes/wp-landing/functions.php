<?php
/**
 * Add Support for Custom Menus
 */

add_theme_support( 'menus' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
add_post_type_support( 'page', 'excerpt' );

/**
 * Register Custom Menu Locations
 */

register_nav_menu( 'top-links', 'Header Links' );
register_nav_menu( 'menu-header', 'Expanded Nav' );
register_nav_menu( 'menu-header-secondary', 'Expanded Nav (Secondary)');
register_nav_menu( 'menu-header-review-links', 'Expanded Nav (Review Links)');

/**
 * Custom Thumbnail Sizes
 *
 * @link http://codex.wordpress.org/Function_Reference/add_image_size
 */

// add_image_size( 'hero', 1600, 500, true );

//Page Slug Body Class
function add_slug_body_class( $classes ) {
  global $post;
  if ( isset( $post ) ) {
    $classes[] = $post->post_name;
  }
  return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


/**
 * Enqueue Theme CSS Files
 */

 function init_enqueue_css() {
  wp_enqueue_style( 'fonts', 'https://use.typekit.net/fzz7uvs.css', array(), null);
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), null);
}

add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

//add SVG to allowed file uploads
function add_file_types_to_uploads($file_types){

  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg';
  $file_types = array_merge($file_types, $new_filetypes );

  return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

/**
 * Enqueue Theme JavaScript Files
 */

add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );

function init_enqueue_js() {
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-custom.js', array(), '3.6.0', false );
  wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.js', array( 'jquery' ), null, true );

  // Localize template URL for usage in JS
  if (is_page('neighborhood') || is_page('contact-us')):
    wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBHqL-wrLO3-Qc42M-A1vaUN_QpkOlabkM', array('jquery'), null, false );
  endif;

  // Localize template URL for usage in JS
  $data = array(
    'template_dir' => get_stylesheet_directory_uri(),
    'lat' => get_field('lat','options'),
    'lng' => get_field('lng','options'),
    'map_link' => get_field('map_link', 'options')
  );

  wp_localize_script( 'app', 'AppData', $data );
}

/**
 * Add Google Analytics Tracking Code
 */

function init_ga_analytics() {
  $propertyID = get_field( 'google_analytics_id', 'option' ); // GA Property ID ?>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129351399-25"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-129351399-25');
  </script>

<?php }

add_action( 'wp_head', 'init_ga_analytics' );


function init_gtm() {
  $GtmID = get_field('tag_manager_id','options');
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','<?= $GtmID ?>');</script>
  <!-- End Google Tag Manager -->
  <?php
}
add_action('wp_head', 'init_gtm');


function init_noscript() { ?>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $GtmID ?>"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <?php
}
add_action('wp_body_open', 'init_noscript');



function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyBHqL-wrLO3-Qc42M-A1vaUN_QpkOlabkM';
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function gform_truncate_phone_numbers( $form ) {
  $fields = $form['fields'];
  foreach ( $form['fields'] as $field ) {
    if ( is_object( $field ) && ( $field->type == 'phone' ) ) {
      $id = $field->id;
      $value = rgpost( 'input_' . $id, true );
      $value = preg_replace('/[^0-9]/', '', $value);
      $_POST['input_' . $id] = $value;
    }
    return $form;
  }
}
//add_filter( 'gform_save_field_value', 'gform_truncate_phone_numbers', -1 );

//add_action( 'gform_before_submission', 'access_entry_via_field', 10, 2 );
function access_entry_via_field( $entry, $form ) {
    foreach ( $form['fields'] as $field ) {
        $inputs = $field->get_entry_inputs();
        if ( is_array( $inputs ) ) {
            foreach ( $inputs as $input ) {
                $value = rgar( $entry, (string) $input['id'] );

            }
        } else {
            $value = rgar( $entry, (string) $field->id );

            $value = preg_replace('/[^0-9]/', '', $value);

            if (is_object($field) && ($field->type == 'phone')):
              $_POST[$input['id']] = $value;
              echo $_POST[$input['id']];
            endif;

        }
    }
}

/**
 * Save phone number as numbers-only for Wood Partners CRM
 */

function convert_phone_number( $value, $entry, $field ) {
  if ( is_object( $field ) && ( $field->type == 'phone' ) ) {
    $value = preg_replace('/[^0-9]/', '', $value);
  }
  return $value;
}
add_filter( 'gform_save_field_value', 'convert_phone_number', 10, 3 );

function padding_top_classes() {
  $section_paddingtop = (!empty(get_sub_field('section_padding_top')) ? get_sub_field('section_padding_top') : '');
  if (!empty($section_paddingtop)):
    if ($section_paddingtop == 'none'):
      echo ' nopadding-top';
    elseif ($section_paddingtop == 'small'):
      echo ' paddingtop-small';
    elseif ($section_paddingtop == 'medium'):
      echo ' paddingtop-medium';
    elseif ($section_paddingtop == 'large'):
      echo ' paddingtop-large';
    elseif ($section_paddingtop == 'xlarge'):
      echo ' paddingtop-xlarge';
    elseif ($section_paddingtop == 'xxlarge'):
      echo ' paddingtop-xxlarge';
    else:
      echo '" style="padding-top:'.$section_paddingtop.'px';
    endif;
  endif;
  }

  function padding_bottom_classes() {
  $section_paddingbottom = (!empty(get_sub_field('section_padding_bottom')) ? get_sub_field('section_padding_bottom') : '');
  if (!empty($section_paddingbottom)):
    if ($section_paddingbottom == 'none'):
      echo ' nopadding-bottom';
    elseif ($section_paddingbottom == 'small'):
      echo ' paddingbottom-small';
    elseif ($section_paddingbottom == 'medium'):
      echo ' paddingbottom-medium';
    elseif ($section_paddingbottom == 'large'):
      echo ' paddingbottom-large';
    elseif ($section_paddingbottom == 'xlarge'):
      echo ' paddingbottom-xlarge';
    elseif ($section_paddingbottom == 'xxlarge'):
      echo ' paddingbottom-xxlarge';
    else:
      echo '" style="padding-bottom:'.$section_paddingbottom.'px';
    endif;
  endif;
}


/**
 * Flatten a multi-dimensional array
 *
 * @param  array  $array  The array to flatten
 * @return array
 */

function array_flatten($array)
{
  $result = [];

  foreach ($array as $element) {
    if (is_array($element)) {
      $result = array_merge($result, array_flatten($element));
    } else {
      $result[] = $element;
    }
  }

  return $result;
}



/**
 * Add theme utility functions
 */

require_once( get_template_directory() . '/includes/functions-defaults.php' );
require_once( get_template_directory() . '/includes/functions-helpers.php' );
require_once( get_template_directory() . '/includes/functions-acf.php' );
require_once( get_template_directory() . '/includes/functions-rentcafe.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo.php' );
require_once( get_template_directory() . '/includes/functions-wordpress-seo-local.php' );
