<?php 

$property_address = get_field('address', 'options');
$show_footer_phone = get_field('show_footer_phone', 'options');
$footer_phone = get_field('footer_phone', 'options');
$site_name = get_bloginfo('name');
$show_wood_partners = get_field('show_wood_partners', 'options');
$wood_partners_logo = get_field('wood_partners_logo', 'options');
$show_wood_residential = get_field('show_wood_residential', 'options');
$wood_residential_logo = get_field('wood_residential_logo', 'options');
$show_wood_pets = get_field('show_wood_pets', 'options');
$wood_pets_logo = get_field('wood_pets_logo', 'options');
$show_wood_housing = get_field('show_wood_housing', 'options');
$wood_housing_logo = get_field('wood_housing_logo', 'options');
$show_wood_accessibility = get_field('show_wood_accessibility','options');
$wood_accessibility_logo = get_field('wood_accessibility_logo', 'options');
$disclosure = get_field('disclosure', 'options');

?>

<footer id="footer" class="footer" role="contentinfo">
  <div class="grid-container footer__info">
    <div class="grid-x align-middle align-center text-center">

      <div class="cell large-auto copyright">

        <p class="address">
          <?php 
            if ($property_address): 
              echo $property_address;
            endif; 
            
            if ($show_footer_phone): 
              echo ' | <a href="tel:'.$footer_phone.'" target="_blank">'.$footer_phone.'</a>';
            endif; 
          ?>
        </p>
        <p class="footer__copyright">
          <span class="copyright">&copy; 2022 Wood Residential, LLC</span> <span class="pipe hide-mobile">|</span> <span class="copyright__name">Professionally Managed by <a href="https://www.woodpartners.com/property-management/" target="_blank" rel="nofollow">Wood Residential</a> <span class="pipe hide-mobile">|</span></span> <span class="copyright__links"><a href="https://www.woodpartners.com/privacy-policy/" target="_blank">Privacy</a> <span class="pipe">|</span> <a href="https://www.woodpartners.com/digital-accessibility/?_ga=2.172898019.1930056624.1579276808-1612636055.1579276808" target="_blank">Digital Accessibility</a></span>
        </p>
      </div>
    </div>
    <div class="grid-x grid-padding-x grid-margin-x grid-padding-x grid-padding-y align-center align-middle">
      <div class="cell small-12 text-center">

        <div class="wood-logos">
          <?php if ($show_wood_partners): ?>
            <a href="https://www.woodpartners.com/" target="_blank"><img class="wood-logo" src="<?= $wood_partners_logo['url']; ?>" width="128" height="35" alt="<?= $wood_pets_logo['alt']; ?>"></a>
          <?php endif; ?>

          <?php if ($show_wood_residential): ?>
            <a href="https://www.woodpartners.com/property-management/" target="_blank"><img class="wood-logo" src="<?= $wood_residential_logo['url']; ?>"  width="128" height="35" alt="<?= $wood_residential_logo['alt']; ?>"></a>
          <?php endif; ?>
        </div>

        <div class="affiliation-logos">
          <?php if ($show_wood_pets): ?>
            <img class="pet-logo" src="<?= $wood_pets_logo['url']; ?>" alt="<?= $wood_pets_logo['alt']; ?>"  width="42" height="21" />
          <?php endif; ?>
          <?php if ($show_wood_housing): ?>
            <img class="fair-logo" src="<?= $wood_housing_logo['url']; ?>" alt="<?= $wood_housing_logo['alt']; ?>" width="26" height="20" />
          <?php endif; ?>

          <?php if ($show_wood_accessibility): ?>
            <img class="access-logo" src="<?= $wood_accessibility_logo['url']; ?>" alt="<?= $wood_accessibility_logo['alt']; ?>"  width="16" height="20" />
          <?php endif; ?>
        </div>
      </div>

      <div class="cell large-11 footer-disclosure">
        <?= ($disclosure ? $disclosure : ''); ?>
      </div> <!-- .cell -->

    </div>
  </footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
